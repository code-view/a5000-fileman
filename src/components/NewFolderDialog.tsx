import React from 'react';

import DirItemForm from './DirItemForm';

import { RawFolder } from '../types';

interface Props {
  open: boolean,
  onCreateFolder: (file: RawFolder) => void,
  onClose: () => void,
};

const NewFolderDialog = ({ open, onCreateFolder, onClose }: Props) => {
  const handleSubmit = (rawItem: RawFolder) => {
    onCreateFolder(rawItem);
  };

  return open ? (
    <DirItemForm
      open={open}
      type="folder"
      onSubmit={handleSubmit}
      onCancel={onClose}
    />
  ) : null;
};

export default NewFolderDialog;
