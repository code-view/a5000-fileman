import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { RawFile, RawFolder, File, DirItemType } from '../types';

interface Props {
  open: boolean,
  type: DirItemType,
  editing?: boolean,
  file?: File,
  onCancel: () => void,
  onSubmit: (item: RawFile | RawFolder) => any,
};

const DirItemForm = (props: Props) => {
  const { open, type, editing, file, onCancel, onSubmit } = props;
  const { name: initialName = '', text: initialText = '' } = file || {};
  
  const [name, setName] = useState(initialName);
  const [text, setText] = useState(initialText);
  const [error, setError] = useState(false);
  
  const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();

    if (name.includes('/')) {
      setError(true);
    } else if (onSubmit) {
      onSubmit(type === 'file' ? { name, text } : { name });
    }
  };
  const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (error && !value.includes('/')) {
      setError(false);
    }
    setName(value);
  };

  return (
    <Dialog open={open} onClose={onCancel} aria-labelledby="form-dialog-title">
      <form onSubmit={handleSubmit}>
        <DialogTitle id="form-dialog-title">
          {`${editing ? 'Change' : 'Create new'} ${type || 'item'}`}
        </DialogTitle>
        <DialogContent>
          <TextField
            required
            autoFocus
            error={error}
            margin="dense"
            id="name"
            label="Name"
            helperText={error && 'Name can\'t contain "/" character'}
            fullWidth
            value={name}
            onChange={handleNameChange}
          />
          {type === 'file' && (
            <TextField
              margin="dense"
              id="filetext"
              label="Text"
              multiline
              rows="4"
              fullWidth
              value={text}
              onChange={(event) => setText(event.target.value)}
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary">
            Cancel
          </Button>
          <Button type="submit" color="primary">
            Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default DirItemForm;
