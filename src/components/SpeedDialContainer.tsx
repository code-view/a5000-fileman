import React, { useState, memo } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import FolderIcon from '@material-ui/icons/Folder';
import Subject from '@material-ui/icons/Subject';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      height: theme.spacing(9),
      width: '100%',
    },
    speedDial: {
      position: 'absolute',
      bottom: theme.spacing(1),
      right: -theme.spacing(1),
    },
  }),
);

type SpeedDialItem = {
  icon: JSX.Element;
  name: string;
  type: 'folder' | 'file';
};

const actions: SpeedDialItem[] = [
  { icon: <FolderIcon />, name: 'New folder', type: 'folder' },
  { icon: <Subject />, name: 'New file', type: 'file' },
];

interface Props {
  onAction: (type: SpeedDialItem['type']) => void,
};

const SpeedDialContainer = ({ onAction }: Props) => {
  const [isOpen, setOpen] = useState(false);
  const classes = useStyles();

  const toggleOpen = () => setOpen(isOpen => !isOpen);
  const handleClose = () => setOpen(false);

  const handleActionClick = (type: SpeedDialItem['type']) => () => {
    setOpen(false);
    if (onAction) {
      onAction(type);
    }
  };

  return (
    <div className={classes.container}>
      <ClickAwayListener onClickAway={handleClose}>
        <SpeedDial
          className={classes.speedDial}
          ariaLabel="SpeedDial to create new file or folder"
          hidden={false}
          icon={<SpeedDialIcon />}
          onClick={toggleOpen}
          onClose={handleClose}
          open={isOpen}
          direction={'up'} // default
        >
          {actions.map(action => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              onClick={handleActionClick(action.type)}
            />
          ))}
        </SpeedDial>
      </ClickAwayListener>
    </div>
  );
};

export default memo(SpeedDialContainer);
