import React from 'react';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Subject from '@material-ui/icons/Subject';
import FolderIcon from '@material-ui/icons/Folder';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import red from '@material-ui/core/colors/red';

import { DirItemType } from '../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      cursor: 'pointer',
    },
    card: {
      width: theme.spacing(11),
      height: theme.spacing(11),
      backgroundColor: (props: Props) => props.selected ? red[100] : '',
    },
    selectedCard: {
      backgroundColor: red[100],
    },
    cardContent: {
      padding: 0,
    },
    icon: {
      width: theme.spacing(11),
      height: theme.spacing(11),
    },
    name: {
      color: (props: Props) => props.selected ? red[200] : '',
      fontSize: 14,
      marginTop: theme.spacing(1),
      maxWidth: theme.spacing(11),
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      textAlign: 'center',
      whiteSpace: 'nowrap',
    },
  })
);

interface Props {
  id: string,
  name: string,
  type: DirItemType,
  text?: string,
  selected: boolean,
  onClick: () => void,
  onDoubleClick: () => void,
}

const DirItem: React.FC<Props> = (props) => {
  const { type, name, onClick, onDoubleClick } = props;
  const classes = useStyles(props);

  return (
    <Box
      className={classes.container}
      margin={2}
      mt={2}
      onClick={onClick}
      onDoubleClick={onDoubleClick}
    >
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          {type === 'file'
            ? (
              <Subject className={classes.icon} color="disabled" />
            )
            : (
              <FolderIcon className={classes.icon} color="disabled" />
            )
          }
        </CardContent>
      </Card>
      <Typography className={classes.name} color="textSecondary">
        {name}
      </Typography>
    </Box>
  );
};

export default DirItem;
