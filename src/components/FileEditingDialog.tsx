import React from 'react';
import { useSelector } from 'react-redux';

import DirItemForm from './DirItemForm';

import { RawFile, RawFolder } from '../types';

import { getFileById } from '../state/selectors';

interface Props {
  fileId: string,
  onSave: (id: string, file: RawFile) => void,
  onClose: () => void,
};

const FileEditingDialog = ({ fileId, onSave, onClose }: Props) => {
  const file = useSelector(getFileById(fileId));
  const handleSubmit = (rawFile: RawFile | RawFolder) => {
    if ('text' in rawFile) {
      onSave(fileId, rawFile);
    }
  };

  return fileId ? (
    <DirItemForm
      open
      editing
      file={file}
      type="file"
      onSubmit={handleSubmit}
      onCancel={onClose}
    />
  ) : null;
};

export default FileEditingDialog;
