import React, { memo } from 'react';
import { useDispatch } from 'react-redux';

import DirItem from './DirItem';

import { DirItemType } from '../types';

import {
  selectDirItem, deselectDirItem, openFolder,
} from '../state/explorer/actions';

interface Props {
  id: string,
  type: DirItemType,
  name: string,
  selected: boolean,
  onDoubleClick: ((id: string) => void) | null,
}

const DirItemContainer: React.FC<Props> = (props) => {
  const { id, type, name, selected, onDoubleClick } = props;
  const dispatch = useDispatch();

  const handleClick = () => {
    if (selected) {
      dispatch(deselectDirItem(id));
    } else {
      dispatch(selectDirItem(id));
    }
  };
  const handleDoubleClick = () => {
    if (type === 'file' && onDoubleClick) {
      onDoubleClick(id);
      return;
    }
    if (type === 'folder') {
      dispatch(openFolder(name));
    }
  };

  return (
    <DirItem
      id={id}
      type={type}
      name={name}
      selected={selected}
      onClick={handleClick}
      onDoubleClick={handleDoubleClick}
    />
  );
};

export default memo(DirItemContainer);
