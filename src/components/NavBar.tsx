import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import Delete from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import { deleteSelectedItems, goUp, goBack } from '../state/explorer/actions';
import {
  getSelectedDirItems, getExplorerPath, getBackList,
} from '../state/selectors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    icon: {
      marginRight: theme.spacing(1),
    },
    deleteIcon: {
      marginRight: theme.spacing(1),
      fontSize: '1.1rem',
    },
    firstButton: {
      paddingLeft: theme.spacing(1),
    },
    upButton: {
      '&:not(:first-child):not(:disabled)': {
        borderLeft: '1px solid rgba(245, 0, 87, 0.5)',
      },
    },
    controlButtonGroup: {
      marginLeft: theme.spacing(2),
    },
  }),
);

const NavBar: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const selectedItems = useSelector(getSelectedDirItems);
  const currentPath = useSelector(getExplorerPath);
  const backList = useSelector(getBackList);

  const handleDeleteClick = () => {
    dispatch(deleteSelectedItems({ ids: selectedItems, currentPath }));
  };
  const handleBackClick = () => dispatch(goBack());
  const handleUpClick = () => dispatch(goUp(currentPath));

  return (
    <Box
      display="flex"
      mt={2}
      p={2}
      px={5}
      border={1}
      borderBottom={0}
      borderColor="grey.300"
    >
      <ButtonGroup
        color="secondary"
        size="medium"
        aria-label="Medium outlined navigation button group"
      >
        <Button
          className={classes.firstButton}
          disabled={backList.length < 1}
          onClick={handleBackClick}
        >
          <NavigateBefore className={classes.icon} />
           Back
        </Button>
        <Button
          className={classes.upButton}
          disabled={currentPath === '/'}
          onClick={handleUpClick}
        >
          <KeyboardArrowUp className={classes.icon} />
          Up
        </Button>
      </ButtonGroup>
      <ButtonGroup
        className={classes.controlButtonGroup}
        disabled={selectedItems.length < 1}
        color="secondary"
        size="medium"
        aria-label="Medium outlined actions button group"
      >
        <Button className={classes.firstButton} onClick={handleDeleteClick}>
          <Delete className={classes.deleteIcon} />
          Delete
        </Button>
      </ButtonGroup>
    </Box>
  );
};

export default NavBar;