import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@material-ui/core/Box';

import DirItemContainer from './DirItemContainer';
import SpeedDialContainer from './SpeedDialContainer';
import NewFileDialog from './NewFileDialog';
import NewFolderDialog from './NewFolderDialog';
import FileEditingDialog from './FileEditingDialog';

import { createFile, updateFile } from '../state/files/actions';
import { createDir } from '../state/directories/actions';
import { getDirectoryContent, getSelectedDirItems } from '../state/selectors';

import { RawFile, RawFolder, DirItemType } from '../types';

const DirectoryViewer: React.FC = () => {
  const [newItemType, setNewItemType] = useState('file');
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [editedFileId, setEditedFileId] = useState('');

  const dispatch = useDispatch();
  const dirContent = useSelector(getDirectoryContent);
  const selectedItems = useSelector(getSelectedDirItems);

  const openNewItemDialog = useCallback(
    (type: DirItemType) => {
      setNewItemType(type);
      setDialogOpen(true);
    },
    [],
  );
  const openEditFileDialog = useCallback(
    (id: string) => setEditedFileId(id),
    [],
  );

  const handleFileCreation = (file: RawFile) => {
    dispatch(createFile(file));
    setDialogOpen(false);
  };
  const handleFolderCreation = (folder: RawFolder) => {
    dispatch(createDir(folder));
    setDialogOpen(false);
  };
  const handleFileUpdate = (priorId: string, file: RawFile) => {
    dispatch(updateFile(priorId, file));
    setEditedFileId('');
  };

  return (
    <>
      <Box
        display="flex"
        flexWrap="wrap"
        p={1}
        px={3}
        border={1}
        borderColor="grey.300"
      >
        {dirContent.map((item) => {
          const { id, type, name } = item;
          return (
            <DirItemContainer
              key={id}
              id={id}
              type={type}
              name={name}
              selected={selectedItems.includes(id)}
              onDoubleClick={type === 'file' ? openEditFileDialog : null}
            />
          );
        })}
        <SpeedDialContainer onAction={openNewItemDialog} />
      </Box>
      <NewFolderDialog
        open={isDialogOpen && newItemType === 'folder'}
        onCreateFolder={handleFolderCreation}
        onClose={() => setDialogOpen(false)}
      />
      <NewFileDialog
        open={isDialogOpen && newItemType === 'file'}
        onCreateFile={handleFileCreation}
        onClose={() => setDialogOpen(false)}
      />
      <FileEditingDialog
        fileId={editedFileId}
        onSave={handleFileUpdate}
        onClose={() => setEditedFileId('')}
      />
    </>
  );
};

export default DirectoryViewer;