import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import { changePath } from '../state/explorer/actions';
import { getExplorerPath } from '../state/selectors';

const { useState, useEffect } = React;

const useStyles = makeStyles(
  createStyles({
    button: {
      textTransform: 'none',
    },
    hiddenRootButton: {
      display: 'none',
    },
    pathField: {
      width: '100%',
    },
  }),
);

// remove one last slash
const removeLastSlash = (path: string) => (
  path.length > 2 && path[path.length - 1] === '/'
    ? path.slice(0, -1)
    : path
);

function usePathState() {
  const pathFromStore = useSelector(getExplorerPath);
  const dispatch = useDispatch();
  const [inputPath, setInputPath] = useState(pathFromStore);

  useEffect(() => {
    setInputPath(pathFromStore);
  }, [pathFromStore]);

  const setStorePath = () => {
    const nextPath = removeLastSlash(inputPath);

    if (nextPath === pathFromStore) {
      return;
    }
    if (!dispatch(changePath(nextPath))) {
      // reset input path value if it was not changed in store
      setInputPath(pathFromStore);
    }
  };

  return {
    inputPath,
    storePath: pathFromStore,
    setInputPath,
    setStorePath,
  };
}

const PathBar: React.FC = () => {
  const classes = useStyles();
  const { inputPath, storePath, setInputPath, setStorePath } = usePathState();

  const handleSubmit = (event: React.SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();
    setStorePath();
  };
  const handlePathChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setInputPath(value);
  };

  return (
    <Box p={1} px={5} border={1} borderBottom={0} borderColor="grey.300">
      <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <TextField
          id="path-field"
          className={classes.pathField}
          margin="dense"
          variant="outlined"
          value={inputPath}
          onChange={handlePathChange}
          onBlur={() => setInputPath(storePath)}
        />
      </form>
    </Box>
  );
};

export default PathBar;
