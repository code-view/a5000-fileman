import React from 'react';

import DirItemForm from './DirItemForm';

import { RawFile, RawFolder } from '../types';

interface Props {
  open: boolean,
  onCreateFile: (file: RawFile) => void,
  onClose: () => void,
};

const NewFileDialog = ({ open, onCreateFile, onClose }: Props) => {
  const handleSubmit = (rawItem: RawFile | RawFolder) => {
    if ('text' in rawItem) {
      onCreateFile(rawItem);
    }
  };

  return open ? (
    <DirItemForm
      open={open}
      type="file"
      onSubmit={handleSubmit}
      onCancel={onClose}
    />
  ) : null;
};

export default NewFileDialog;
