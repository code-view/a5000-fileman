export type DirItemType = 'file' | 'folder';

export interface RawFile {
  name: string,
  text: string,
};

export interface RawFolder {
  name: string,
};

export interface File {
  id: string,
  type: DirItemType,
  name: string,
  text: string,
};

export interface Folder {
  id: string,
  type: DirItemType,
  name: string,
  content: string[],
};
