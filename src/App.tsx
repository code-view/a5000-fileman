import React from 'react';
import Container from '@material-ui/core/Container';
import NavBar from './components/NavBar';
import PathBar from './components/PathBar';
import DirectoryViewer from './components/DirectoryViewer';

const App: React.FC = () => {
  return (
    <Container maxWidth="md">
      <NavBar />
      <PathBar />
      <DirectoryViewer />
    </Container>
  );
};

export default App;
