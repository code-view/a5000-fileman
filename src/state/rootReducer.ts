import { combineReducers } from 'redux';

import explorer from './explorer/reducer';
import directories from './directories/reducer';
import files from './files/reducer';

export default combineReducers({
  explorer,
  directories,
  files,
});
