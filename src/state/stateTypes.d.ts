import { ExplorerState } from './explorer/reducer';
import { FilesState } from './files/reducer';
import { DirectoriesState } from './directories/reducer';

export interface State {
  explorer: ExplorerState,
  files: FilesState,
  directories: DirectoriesState,
};
