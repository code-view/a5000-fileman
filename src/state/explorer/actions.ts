import { createAction } from 'redux-starter-kit';
import { Dispatch } from 'redux';

import { State } from '../stateTypes';

import * as aT from '../actionTypes';

import { getDirByPathId } from '../selectors';

export const selectDirItem = createAction(aT.SELECT_DIR_ITEM);

export const deselectDirItem = createAction(aT.DESELECT_DIR_ITEM);

export const deleteSelectedItems = createAction(aT.DELETE_SELECTED_ITEMS);

export const openFolder = createAction(aT.OPEN_FOLDER);

export const goUp = createAction(aT.GO_UP);

export const goBack = createAction(aT.GO_BACK);

export const changePathOK = createAction(aT.CHANGE_PATH_OK);
export const changePathFail = createAction(aT.CHANGE_PATH_FAIL);

export const changePath = (path: string) => (
  (dispatch: Dispatch, getState: () => State) => {
    const directory = getDirByPathId(path)(getState());
    if (directory) {
      dispatch(changePathOK(path));
      return true;
    }
    dispatch(changePathFail({
      path,
      message: `Can't find "${path}". Try other path`,
    }));
    return false;
  }
);
