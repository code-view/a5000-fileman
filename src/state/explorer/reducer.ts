import { createReducer } from 'redux-starter-kit';

import * as aT from '../actionTypes';
import {
  addIfNotExist, makeOneLevelUpPath, joinPath, findIdsOnPath,
} from '../helpers';

export interface ExplorerState {
  path: string,
  selected: string[],
  backList: Array<string>,
};

const MAX_BACK_ITEMS = 10;

const initialState: ExplorerState = {
  path: '/',
  selected: [],
  backList: [],
};

const makeEmptyIfNot = (arr: string[]) => (arr.length < 1 ? arr : []);

const removeItemsByPath = (includeSame: boolean = true) => (
  (items: string[], path: string) => {
    const idsToRemove = findIdsOnPath(items, path, { includeSame });
    return items.filter(item => !idsToRemove.includes(item));
  }
);

const correctBackEntries = (backList: string[], currentPath: string) => {
  // remove nearby duplicates
  const list = backList.filter((item, index) => item !== backList[index + 1]);
  // make sure that last item is not same as current path
  return list[list.length - 1] === currentPath ? list.slice(0, -1) : list;
};

const explorerReducer = createReducer(initialState, {
  [aT.SELECT_DIR_ITEM]: (state, action) => ({
    ...state,
    selected: addIfNotExist(state.selected, action.payload),
  }),

  [aT.DESELECT_DIR_ITEM]: (state, action) => ({
    ...state,
    selected: state.selected.filter(id => id !== action.payload),
  }),

  [aT.DELETE_SELECTED_ITEMS]: (state, { payload }) => {
    // to remove from backList all deleted dirs and their nested dirs
    const list = payload.ids.reduce(removeItemsByPath(), [...state.backList]);
    return {
      ...state,
      selected: [],
      backList: correctBackEntries(list, state.path),
    };
  },

  [aT.CREATE_DIR]: (state, action) => {
    const { createdDir } = action.payload;
    const list = removeItemsByPath(false)(state.backList, createdDir.id);
    return {
      ...state,
      backList: correctBackEntries(list, state.path),
    };
  },

  [aT.OPEN_FOLDER]: (state, action) => {
    const { path, backList, selected } = state;
    return {
      ...state,
      path: joinPath(path, action.payload),
      selected: makeEmptyIfNot(selected),
      backList: backList.concat(path).slice(-MAX_BACK_ITEMS),
    };
  },

  [aT.GO_UP]: (state) => {
    const { path, backList, selected } = state;
    if (path === '/') {
      return state;
    }
    return {
      ...state,
      path: makeOneLevelUpPath(path),
      selected: makeEmptyIfNot(selected),
      backList: backList.concat(path).slice(-MAX_BACK_ITEMS),
    };
  },

  [aT.GO_BACK]: (state) => {
    const { backList, selected } = state;
    if (backList.length < 1) {
      return state;
    }
    return {
      ...state,
      path: backList[backList.length - 1], // set path to last backList entry
      selected: makeEmptyIfNot(selected),
      backList: backList.slice(0, -1),
    };
  },

  [aT.CHANGE_PATH_OK]: (state, action) => {
    const { path, backList, selected } = state;
    if (path === action.payload) { // next path is same as current
      return state;
    }
    return {
      ...state,
      path: action.payload,
      selected: makeEmptyIfNot(selected),
      backList: backList.concat(path).slice(-MAX_BACK_ITEMS),
    };
  },
});

export default explorerReducer;
