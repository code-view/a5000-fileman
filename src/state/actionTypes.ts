export const CREATE_FILE = 'files--CREATE_FILE';

export const UPDATE_FILE = 'files--UPDATE_FILE';

export const CREATE_DIR = 'directories--CREATE_DIR';

export const SELECT_DIR_ITEM = 'explorer--SELECT_DIR_ITEM';

export const DESELECT_DIR_ITEM = 'explorer--DESELECT_DIR_ITEM';

export const DELETE_SELECTED_ITEMS = 'explorer--DELETE_SELECTED_ITEMS';

export const OPEN_FOLDER = 'explorer--OPEN_FOLDER';

export const GO_UP = 'explorer--GO_UP';

export const GO_BACK = 'explorer--GO_BACK';

export const CHANGE_PATH_OK = 'explorer--CHANGE_PATH_OK';

export const CHANGE_PATH_FAIL = 'explorer--CHANGE_PATH_FAIL';
