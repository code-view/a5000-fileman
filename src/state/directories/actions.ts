import { Dispatch } from 'redux';

import { RawFolder } from '../../types';
import { State } from '../stateTypes';

import * as aT from '../actionTypes';
import { getExplorerPath } from '../selectors';
import { joinPath } from '../helpers';

export const createDir = (folder: RawFolder) => (
  (dispatch: Dispatch, getState: () => State) => {
    const currentPath = getExplorerPath(getState());
    const createdDir = {
      ...folder,
      id: joinPath(currentPath, folder.name),
      type: 'folder',
      content: [],
    };

    return dispatch({
      type: aT.CREATE_DIR,
      payload: {
        currentPath,
        createdDir,
      },
    });
  }
);
