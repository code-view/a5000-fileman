import { createReducer } from 'redux-starter-kit';

import * as aT from '../actionTypes';

import { Folder } from '../../types';
import { findIdsOnPath, deleteItemsByIds } from '../helpers';

export interface DirectoriesState {
  [pathID: string]: Folder
};

export const ROOT_ID = '/';

// NOTE: paths are used as ids of directory objects

const initialState: DirectoriesState = {
  [ROOT_ID]: {
    id: ROOT_ID,
    type: 'folder',
    name: 'ROOT',
    content: [
      '/Folder 1',
      '/Test file.txt',
    ],
  },
  '/Folder 1': {
    id: '/Folder 1',
    type: 'folder',
    name: 'Folder 1',
    content: ['/Folder 1/Another dir', '/Folder 1/File 2.txt'],
  },
  '/Folder 1/Another dir': {
    id: '/Folder 1/Another dir',
    type: 'folder',
    name: 'Another dir',
    content: ['/Folder 1/Another dir/Third File.txt'],
  },
};

const addIdToDirContent = (currentDir: Folder, itemId: string) => {
  if (currentDir.content.includes(itemId)) {
    return currentDir;
  }
  return {
    ...currentDir,
    content: currentDir.content.concat(itemId),
  };
};

const replaceIdInDirContent = (
  currentDir: Folder,
  priorId: string,
  newId: string,
) => {
  const priorIdIndex = currentDir.content.indexOf(priorId);
  const updatedContent = currentDir.content.filter((id, index) => {
    return id !== newId || index === priorIdIndex; // remove duplicated ids
  });
  updatedContent[priorIdIndex] = newId;
  return {
    ...currentDir,
    content: updatedContent,
  };
};

const removeIdsFromDirContent = (currentDir: Folder, ids: string[]) => {
  return {
    ...currentDir,
    content: currentDir.content.filter(id => !ids.includes(id)),
  };
};

const deleteNestedDirsByPath = (dirs: DirectoriesState, path: string) => {
  const matchingPathIds = findIdsOnPath(Object.keys(dirs), path);
  return deleteItemsByIds(dirs, matchingPathIds);
};

const directoriesReducer = createReducer(initialState, {
  [aT.CREATE_FILE]: (state, action) => {
    const { currentPath, createdFile: { id } } = action.payload;
    const updatedCurrentDir = addIdToDirContent(state[currentPath], id);
    return {
      ...state,
      [currentPath]: updatedCurrentDir,
    };
  },

  [aT.CREATE_DIR]: (state, action) => {
    const { currentPath, createdDir } = action.payload;
    const currentDir = state[currentPath];
    const dirWithSamePath = state[createdDir.id];
    // if folder with same name as created one exists in current dir, its
    // nested dirs should be removed because of rewrite
    const nextState = dirWithSamePath
      ? deleteNestedDirsByPath({ ...state }, dirWithSamePath.id)
      : { ...state };
    nextState[createdDir.id] = createdDir;
    nextState[currentPath] = addIdToDirContent(currentDir, createdDir.id);
    return nextState;
  },

  [aT.UPDATE_FILE]: (state, action) => {
    const { currentPath, priorId, nextId } = action.payload;
    // if file id is not changed, no need to update current dir "content"
    if (priorId === nextId) return state;
    const currentDir = state[currentPath];
    return {
      ...state,
      // update currenty opened directory content
      [currentPath]: replaceIdInDirContent(currentDir, priorId, nextId),
    };
  },

  [aT.DELETE_SELECTED_ITEMS]: (state, action) => {
    const { ids, currentPath } = action.payload;
    const nextState = { ...state };
    const dirIds = Object.keys(state);
    ids.forEach((pathId: string) => {
      // omit selected files and remove dirs nested in selected dirs
      if (state[pathId]) {
        const matchingIds = findIdsOnPath(dirIds, pathId);
        deleteItemsByIds(nextState, matchingIds);
        delete nextState[pathId]; // remove this selected dir itself
      }
    });
    // remove ids of deleted items from current dir content
    nextState[currentPath] = removeIdsFromDirContent(state[currentPath], ids);
    return nextState;
  },
});

export default directoriesReducer;
