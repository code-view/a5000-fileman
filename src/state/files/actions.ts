import { Dispatch } from 'redux';

import { RawFile } from '../../types';
import { State } from '../stateTypes';

import * as aT from '../actionTypes';
import { getExplorerPath } from '../selectors';
import { joinPath } from '../helpers';

export const createFile = (file: RawFile) => (
  (dispatch: Dispatch, getState: () => State) => {
    const currentPath = getExplorerPath(getState());
    const pathID = joinPath(currentPath, `${file.name}.txt`);

    return dispatch({
      type: aT.CREATE_FILE,
      payload: {
        currentPath,
        createdFile: {
          ...file,
          id: pathID,
          type: 'file',
        },
      },
    });
  }
);

export const updateFile = (priorId: string, rawFile: RawFile) => (
  (dispatch: Dispatch, getState: () => State) => {
    const state = getState();
    const currentPath = getExplorerPath(state);
    const nextId = joinPath(currentPath, `${rawFile.name}.txt`);

    return dispatch({
      type: aT.UPDATE_FILE,
      payload: {
        priorId,
        nextId,
        fileUpdates: { ...rawFile, id: nextId },
        currentPath,
      },
    });
  }
);
