import { createReducer } from 'redux-starter-kit';

import * as aT from '../actionTypes';

import { File } from '../../types';

import { findIdsOnPath, deleteItemsByIds } from '../helpers';

export interface FilesState {
  [pathID: string]: File
};

// NOTE: paths are used as ids of file objects

const initialState: FilesState = {
  '/Test file.txt': {
    id: '/Test file.txt',
    type: 'file',
    name: 'Test file',
    text: 'Whatever',
  },
  '/Folder 1/File 2.txt': {
    id: '/Folder 1/File 2.txt',
    type: 'file',
    name: 'File 2',
    text: 'Any text',
  },
  '/Folder 1/Another dir/Third File.txt': {
    id: '/Folder 1/Another dir/Third File.txt',
    type: 'file',
    name: 'Third File',
    text: 'Text of the file',
  },
};

const deleteFilesByPath = (files: FilesState, path: string) => {
  const matchingPathIds = findIdsOnPath(Object.keys(files), path);
  if (matchingPathIds.length < 1) {
    return files;
  }
  const nextFiles = { ...files };
  return deleteItemsByIds(nextFiles, matchingPathIds);
};

const filesReducer = createReducer(initialState, {
  [aT.CREATE_FILE]: (state, action) => {
    const { createdFile } = action.payload;
    return {
      ...state,
      [createdFile.id]: createdFile,
    };
  },

  [aT.CREATE_DIR]: (state, action) => {
    const { createdDir } = action.payload;
    // if folder with same name as created one exists in current dir, its
    // nested files should be removed because of rewrite
    return deleteFilesByPath(state, createdDir.id);
  },

  [aT.UPDATE_FILE]: (state, action) => {
    const { priorId, nextId, fileUpdates } = action.payload;
    const existingFile = state[priorId];
    const nextState = { ...state };
    delete nextState[priorId];
    nextState[nextId] = { ...existingFile, ...fileUpdates };
    return nextState;
  },

  [aT.DELETE_SELECTED_ITEMS]: (state, action) => {
    const nextState = { ...state };
    const fileIds = Object.keys(state);
    action.payload.ids.forEach((pathId: string) => {
      // omit files and process only dirs here, to remove their nested files
      if (state[pathId]) {
        return;
      }
      const matchingIds = findIdsOnPath(fileIds, pathId); // pathId - id of dir
      deleteItemsByIds(nextState, matchingIds);
    });
    // delete selected files
    return deleteItemsByIds(nextState, action.payload.ids);
  },
});

export default filesReducer;
