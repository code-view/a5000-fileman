import { createSelector } from 'redux-starter-kit';

import { State } from './stateTypes';
import { Folder, File } from '../types';

export const getFiles = (state: State) => state.files;

export const getFileById = (id: string) => (state: State) => (
  getFiles(state)[id]
);

export const getDirectories = (state: State) => state.directories;

export const getDirByPathId = (path: string) => (state: State) => (
  getDirectories(state)[path]
);

export const getExplorerPath = (state: State) => state.explorer.path;

export const getSelectedDirItems = (state: State) => state.explorer.selected;

export const getBackList = (state: State) => state.explorer.backList;

export const getCurrentDir = createSelector(
  [getExplorerPath, getDirectories],
  (explorerPath, dirs) => {
    return dirs[explorerPath];
  },
);

export const getCurrentDirContent = (state: State) => (
  getCurrentDir(state).content
);

export const getDirectoryContent = createSelector(
  [getFiles, getDirectories, getCurrentDirContent],
  (files, dirs, idsOfDirItems) => {
    const nestedFolders: Folder[] = [];
    const nestedFiles: File[] = [];

    idsOfDirItems.forEach((id: string) => {
      if (dirs[id]) {
        nestedFolders.push(dirs[id]);
        return;
      }
      if (files[id]) {
        nestedFiles.push(files[id]);
      }
    });
    return [...nestedFolders, ...nestedFiles];
  },
);
