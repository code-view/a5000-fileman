import { configureStore, getDefaultMiddleware } from 'redux-starter-kit';
import { createLogger } from 'redux-logger';

import rootReducer from './rootReducer';

// must be the last middleware in the chain
const logger = createLogger({
  duration: true,
});

const middleware = getDefaultMiddleware();

const isDevelopment = process.env.NODE_ENV === 'development';

export default function createReduxStore(preloadedState = {}) {
  const store = configureStore({
    reducer: rootReducer,
    middleware: isDevelopment ? [...middleware, logger] : middleware,
    preloadedState,
    devTools: isDevelopment,
  });
  return store;
}
