export const addIfNotExist = (arr: Array<string>, item: string) => {
  if (arr.includes(item)) {
    return arr;
  }
  return arr.concat(item);
};

export const makeOneLevelUpPath = (path: string) => {
  if (path === '/') {
    return '/';
  }
  const lastSlashIndex = path.lastIndexOf('/');
  return lastSlashIndex === 0 ? '/' : path.slice(0, lastSlashIndex);
};

export const joinPath = (path: string, name: string) => {
  return `${path === '/' ? '' : path}/${name}`;
};

interface Opts {
  includeSame: boolean
}

export const findIdsOnPath = (pathIds: string[], path: string, opts?: Opts) => {
  const { includeSame = false } = opts || {};
  return pathIds.filter((pathId) => {
    return includeSame
      ? pathId.indexOf(`${path}/`) === 0 || pathId === path
      : pathId.indexOf(`${path}/`) === 0;
  });
};

export const deleteItemsByIds = (items: any, ids: string[]) => {
  ids.forEach((id) => {
    if (items[id]) {
      delete items[id];
    }
  });
  return items;
};
